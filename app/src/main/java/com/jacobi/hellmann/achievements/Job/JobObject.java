package com.jacobi.hellmann.achievements.Job;

import com.jacobi.hellmann.achievements.CommonObjects.DriversLicense;

import java.util.ArrayList;

public class JobObject {
    private int id;
    private String state;
    private String expiresAt;
    private String publishedAt;
    ArrayList<String> audience = new ArrayList<>();
    private String title;
    private String employmentType;
    private String applicationType;

    public String getDescription() {
        return description;
    }

    private String description;
    private int contactId;
    private double workingHours;
    private boolean availableNow;
    Payment Payment;
    PaymentPerHour paymentPerHour;
    PaymentPerMonth PaymentPerMonth;
    private String paymentRate;
    private boolean withCommission;
    private boolean onAccount;
    private boolean onTaxCard;
    ArrayList<Integer> studyFieldIds = new ArrayList<>();
    ArrayList<Integer> languageIds = new ArrayList<>();
    ArrayList<Integer> computerKnowledgeIds = new ArrayList<>();
    ArrayList<Integer> certificateIds = new ArrayList<>();
    DriversLicense driversLicense;
    private boolean withoutLocations;
    ArrayList<Location> locations = new ArrayList<>();
    private String updatedAt;
    private int objectVersion;
    private String publicUrl;

    public JobObject(int id, String state, String expiresAt, String publishedAt,
                     ArrayList<String> audience, String title, String employmentType,
                     String applicationType, String description, int contactId,
                     double workingHours, boolean availableNow, Payment payment,
                     PaymentPerHour paymentPerHour, PaymentPerMonth paymentPerMonth,
                     String paymentRate, boolean withCommission, boolean onAccount, boolean onTaxCard,
                     ArrayList<Integer> studyFieldIds, ArrayList<Integer> languageIds,
                     ArrayList<Integer> computerKnowledgeIds, ArrayList<Integer> certificateIds,
                     DriversLicense driversLicense, boolean withoutLocations,
                     ArrayList<Location> locations, String updatedAt, int objectVersion, String publicUrl) {

        this.id = id;
        this.state = state;
        this.expiresAt = expiresAt;
        this.publishedAt = publishedAt;
        this.audience = audience;
        this.title = title;
        this.employmentType = employmentType;
        this.applicationType = applicationType;
        this.description = description;
        this.contactId = contactId;
        this.workingHours = workingHours;
        this.availableNow = availableNow;
        Payment = payment;
        this.paymentPerHour = paymentPerHour;
        PaymentPerMonth = paymentPerMonth;
        this.paymentRate = paymentRate;
        this.withCommission = withCommission;
        this.onAccount = onAccount;
        this.onTaxCard = onTaxCard;
        this.studyFieldIds = studyFieldIds;
        this.languageIds = languageIds;
        this.computerKnowledgeIds = computerKnowledgeIds;
        this.certificateIds = certificateIds;
        this.driversLicense = driversLicense;
        this.withoutLocations = withoutLocations;
        this.locations = locations;
        this.updatedAt = updatedAt;
        this.objectVersion = objectVersion;
        this.publicUrl = publicUrl;
    }

    public boolean getWithoutLocations() {
        return withoutLocations;
    }

    public String getLocationsCity(){

        if(getWithoutLocations())
            return "";

        StringBuilder cityList = new StringBuilder(50);
        for(int i = 0; i < locations.size(); i++) {
            cityList.append(locations.get(i).getCity() + " ");
        }

        return cityList.toString();
    }


    public String getExpiresAt() {
        return expiresAt;
    }

    public String getTitle() {
        return title;
    }

    public String getWorkingHoursString() {
        return String.valueOf(workingHours);
    }

    public PaymentPerHour getPaymentPerHour() {
        return paymentPerHour;
    }

    public String getPaymentPerHourString() {
        return String.valueOf(paymentPerHour);
    }
}
