package com.jacobi.hellmann.achievements.Ui;

import android.app.Dialog;
import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jacobi.hellmann.achievements.Util.AchievementsHandler;
import com.jacobi.hellmann.achievements.R;

public class JobDetailPopUp extends Dialog {


    private TextView title;
    private TextView description;
    private Button apply;

    public JobDetailPopUp(Context context) {
        super(context);

        setContentView(R.layout.popup_detail_job_listing);
        this.title = findViewById(R.id.job_popup_title);
        this.description = findViewById(R.id.job_popup_description);
        apply = findViewById(R.id.job_popup_apply);
        this.apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AchievementsHandler(getContext()).completeFirst();
                new CustomToast(getContext());
                dismiss();
            }
        });
    }

    public void setupPopUp(String title, String description) {
        this.title.setText(title);
        this.description.setText(Html.fromHtml(description));
    }
}
