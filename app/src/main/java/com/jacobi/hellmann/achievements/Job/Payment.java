package com.jacobi.hellmann.achievements.Job;

public class Payment {
    private int amount;
    private String currency;

    public Payment(int amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }
}