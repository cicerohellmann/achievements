package com.jacobi.hellmann.achievements.Ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.jacobi.hellmann.achievements.Job.JobObject;
import com.jacobi.hellmann.achievements.Adapter.JobListingAdapter;
import com.jacobi.hellmann.achievements.Util.JsonParser;
import com.jacobi.hellmann.achievements.R;
import com.jacobi.hellmann.achievements.Student.StudentObject;
import java.util.ArrayList;

public class JobListingActivity extends BaseActivity {

    private ArrayList<JobObject> jobList = new ArrayList<>();
    private RecyclerView jobListRecyclerView;
    private JobListingAdapter jobListAdapter;
    private StudentObject student;
    private RecyclerView.LayoutManager jobListLayoutManager;
    JobDetailPopUp myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addContentView(R.layout.activity_job_listing);
        init();

    }

    public void init(){
        setupRecyclerView();
        loadData();
    }



    public void setJobList(ArrayList<JobObject> jobList) {
        this.jobList.addAll(jobList);
    }
    public RecyclerView.Adapter getJobListAdapter(){
        return jobListAdapter;
    }

    public void loadData(){
        JsonParser jasonParser = new JsonParser(this, "jobs_100.json");

        jasonParser.setJobLoadDataListener(new JsonParser.JobEventListener(){
            @Override
            public void onFinishLoadingJobListEvent(ArrayList<JobObject> jobList) {
                setJobList(jobList);
                getJobListAdapter().notifyDataSetChanged();
            }

        });

        jasonParser.loadJobList();
    }

    public void setupRecyclerView(){

        jobListRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_jobs);
        jobListRecyclerView.setHasFixedSize(true);

        jobListLayoutManager = new LinearLayoutManager(this);
        jobListRecyclerView.setLayoutManager(jobListLayoutManager);

        jobListAdapter = new JobListingAdapter(jobList);
        jobListRecyclerView.setAdapter(jobListAdapter);


        jobListAdapter.setOnItemClickListener(new JobListingAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                jobList.get(position);
                myDialog = new JobDetailPopUp(getContext());
                myDialog.setupPopUp(jobList.get(position).getTitle(), jobList.get(position).getDescription());
                myDialog.show();
            }
        });
    }

    public Context getContext(){
        return this;
    }
}
