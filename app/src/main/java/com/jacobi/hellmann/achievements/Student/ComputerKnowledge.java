package com.jacobi.hellmann.achievements.Student;

public class ComputerKnowledge {
    private int id;
    private String type;
    private String level;

    public ComputerKnowledge(int id, String type, String level) {
        this.id = id;
        this.type = type;
        this.level = level;
    }
}
