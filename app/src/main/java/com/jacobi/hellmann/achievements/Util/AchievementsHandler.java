package com.jacobi.hellmann.achievements.Util;

import android.content.Context;

import com.jacobi.hellmann.achievements.Ui.CustomToast;

public class AchievementsHandler {
    Context context;
    SharedPreferenceHandler sph;

    public AchievementsHandler(Context context) {
        this.context = context;
        this.sph = new SharedPreferenceHandler(context);
    }

    public void completeFirst() {
        if (!sph.isCompletedFirst()) {
            sph.setUserAchievementFirstComplete();
        }
    }

    public boolean isCompletedFirst() {
        return sph.isCompletedFirst();
    }
}
