package com.jacobi.hellmann.achievements.Util;

import android.content.Context;
import android.content.SharedPreferences;

import com.jacobi.hellmann.achievements.Student.StudentObject;

public class SharedPreferenceHandler {
    public static final String USER_INFO = "userInfo";
    public static final String BIRTHDAY = "birthday";
    public static final String EMAIL = "email";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String GENDER = "gender";
    public static final String MOBILE_NUMBER = "mobileNumber";
    public static final String ACHIEVEMENT_FIRST = "achievementFirst";

    private Context context;
    private SharedPreferences settings;
    SharedPreferences.Editor editor;

    public SharedPreferenceHandler(Context context) {
        this.context = context;
        settings = context.getSharedPreferences(SharedPreferenceHandler.USER_INFO, 0);
        editor = settings.edit();
    }

    public void setUserInfo(StudentObject student){
        editor.putString(BIRTHDAY,student.getBirthday());
        editor.putString(EMAIL, student.getEmail());
        editor.putString(FIRST_NAME, student.getFirstName());
        editor.putString(GENDER, student.getGender());
        editor.putString(LAST_NAME, student.getLastName());
        editor.putString(MOBILE_NUMBER, student.getMobileNumber());

        if(!settings.contains(ACHIEVEMENT_FIRST))
            editor.putBoolean(ACHIEVEMENT_FIRST, false);

        editor.commit();
    }

    public String getUserBirthday(){
        return settings.getString(BIRTHDAY, "");
    }
    public String getUserEmail(){
        return settings.getString(EMAIL, "");
    }
    public String getUserName(){
        return settings.getString(FIRST_NAME, "") + " " + settings.getString(LAST_NAME, "");
    }
    public String getUserGender(){
        return settings.getString(GENDER, "");
    }
    public String getUserMobileNumber(){
        return settings.getString(MOBILE_NUMBER, "");
    }

    public void setUserAchievementFirstComplete(){
        editor.putBoolean(ACHIEVEMENT_FIRST, true);
        editor.commit();
    }

    public boolean isCompletedFirst(){
        return settings.getBoolean(ACHIEVEMENT_FIRST, false);
    }
}
