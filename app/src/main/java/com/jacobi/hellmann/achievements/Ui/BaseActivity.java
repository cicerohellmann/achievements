package com.jacobi.hellmann.achievements.Ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.jacobi.hellmann.achievements.R;
import com.jacobi.hellmann.achievements.Util.SharedPreferenceHandler;

public class BaseActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_drawer_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);

        setupNavView();

        toggle.syncState();
    }

    public void setupNavView() {

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.menu_job_listing) {
                    Intent i = new Intent(getApplicationContext(), JobListingActivity.class);
                    getApplicationContext().startActivity(i);

                } else if (id == R.id.menu_user) {
                    Intent i = new Intent(getApplicationContext(), UserActivity.class);
                    getApplicationContext().startActivity(i);

                } else if (id == R.id.menu_achievements) {
                    Intent i = new Intent(getApplicationContext(), AchievementsActivity.class);
                    getApplicationContext().startActivity(i);

                } else if (id == R.id.menu_logout) {
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    getApplicationContext().startActivity(i);
                    finish();

                }

                drawerLayout.closeDrawers();
                return true;
            }
        });

        View headerView = navigationView.getHeaderView(0);

        SharedPreferenceHandler sph = new SharedPreferenceHandler(this);
        TextView userName = (TextView) headerView.findViewById(R.id.nav_user_name);
        userName.setText(sph.getUserName());
    }

    public void addContentView(int layoutId) {
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(layoutId, null, false);
        drawerLayout.addView(contentView, 0);
    }
}