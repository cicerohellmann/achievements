package com.jacobi.hellmann.achievements.Util;

import android.app.Activity;
import android.content.Context;

import com.jacobi.hellmann.achievements.CommonObjects.DriversLicense;
import com.jacobi.hellmann.achievements.Job.JobObject;
import com.jacobi.hellmann.achievements.Job.Location;
import com.jacobi.hellmann.achievements.Job.Payment;
import com.jacobi.hellmann.achievements.Job.PaymentPerHour;
import com.jacobi.hellmann.achievements.Job.PaymentPerMonth;
import com.jacobi.hellmann.achievements.Student.Address;
import com.jacobi.hellmann.achievements.Student.ComputerKnowledge;
import com.jacobi.hellmann.achievements.Student.Language;
import com.jacobi.hellmann.achievements.Student.ProfessionalExperience;
import com.jacobi.hellmann.achievements.Student.Resume;
import com.jacobi.hellmann.achievements.Student.StudentObject;
import com.jacobi.hellmann.achievements.Util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class JsonParser {
    public interface JobEventListener {
        void onFinishLoadingJobListEvent(ArrayList<JobObject> jobList);
    }

    public interface StudentEventListener {
        void onFinishLoadingStudentDataEvent(StudentObject student);
    }

    Context ctx;
    String jsonPath;
    private JobEventListener finishedLoadingJobDataListener;
    private StudentEventListener finishedLoadingStudentDataListener;

    public JsonParser(Context ctx, String jsonPath) {
        this.ctx = ctx;
        this.jsonPath = jsonPath;
        this.finishedLoadingJobDataListener = null;
        this.finishedLoadingStudentDataListener = null;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = ctx.getAssets().open(jsonPath);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void loadJobList() {

        String jsonJobList = loadJSONFromAsset();
        ArrayList<JobObject> jobList = new ArrayList<>();

        try {
            JSONArray jsonarray = new JSONArray(jsonJobList);

            for (int i = 0; i < jsonarray.length(); i++) {

                JSONObject jobJson = jsonarray.getJSONObject(i);

                JSONObject paymentJson = jobJson.getJSONObject("payment");
                Payment payment = new Payment(paymentJson.getInt("amount"), paymentJson.getString("currency"));

                JSONObject paymentPerHourJson = jobJson.getJSONObject("payment_per_hour");
                PaymentPerHour paymentPerHour = new PaymentPerHour(paymentPerHourJson.getInt("amount"), paymentPerHourJson.getString("currency"));

                JSONObject paymentPerMonthJson = jobJson.getJSONObject("payment_per_month");
                PaymentPerMonth paymentPerMonth = new PaymentPerMonth(paymentPerMonthJson.getInt("amount"), paymentPerMonthJson.getString("currency"));

                JSONObject driversLicenseJson = jobJson.getJSONObject("drivers_licence");
                DriversLicense driversLicense = new DriversLicense(driversLicenseJson.getBoolean("car"), driversLicenseJson.getBoolean("truck"),
                        driversLicenseJson.getBoolean("motorcycle"), driversLicenseJson.getBoolean("scooter"),
                        driversLicenseJson.getBoolean("has_car"), driversLicenseJson.getBoolean("passenger_transportation"),
                        driversLicenseJson.getBoolean("trailer"), driversLicenseJson.getBoolean("forklift"));

                JSONArray audienceJson = jobJson.getJSONArray("audience");
                ArrayList<String> audienceArray = new ArrayList<>();
                for (int a = 0; a < audienceJson.length(); a++) {
                    audienceArray.add(audienceJson.getString(a));
                }
                JSONArray studyFieldIdsJson = jobJson.getJSONArray("study_field_ids");
                ArrayList<Integer> studyFieldIdArray = new ArrayList<>();
                for (int s = 0; s < studyFieldIdsJson.length(); s++) {
                    studyFieldIdArray.add(studyFieldIdsJson.getInt(s));
                }
                JSONArray languageIdsJson = jobJson.getJSONArray("language_ids");
                ArrayList<Integer> languageIdArray = new ArrayList<>();
                for (int l = 0; l < languageIdsJson.length(); l++) {
                    languageIdArray.add(languageIdsJson.getInt(l));
                }
                JSONArray computerKnowledgeIdsJson = jobJson.getJSONArray("computer_knowledge_ids");
                ArrayList<Integer> computerKnowledgeIdsArray = new ArrayList<>();
                for (int c = 0; c < computerKnowledgeIdsJson.length(); c++) {
                    computerKnowledgeIdsArray.add(computerKnowledgeIdsJson.getInt(c));
                }
                JSONArray certificateIdsJson = jobJson.getJSONArray("certificate_ids");
                ArrayList<Integer> certificateIdsArray = new ArrayList<>();
                for (int ci = 0; ci < certificateIdsJson.length(); ci++) {
                    certificateIdsArray.add(certificateIdsJson.getInt(ci));
                }
                JSONArray locationsJson = jobJson.getJSONArray("locations");
                ArrayList<Location> locationsArray = new ArrayList<>();
                for (int lo = 0; lo < locationsJson.length(); lo++) {

                    JSONObject jsonLocationObject = locationsJson.getJSONObject(lo);
                    //String district = jsonLocationObject.getString("district"); in some occurrences of the object "location" district doesn't exist.
                    locationsArray.add(new Location(jsonLocationObject.getInt("id"), jsonLocationObject.getString("city"),
                            "", jsonLocationObject.getString("country"),
                            jsonLocationObject.getString("postal_code")));

                }

                JobObject jobObj;
                jobObj = new JobObject(jobJson.getInt("id"), jobJson.getString("state"),
                        new Date(jobJson.getString("expires_at")).dateConversion(), jobJson.getString("published_at"),
                        audienceArray, jobJson.getString("title"), jobJson.getString("employment_type"),
                        jobJson.getString("application_type"), jobJson.getString("description"),
                        jobJson.getInt("contact_id"), jobJson.getDouble("working_hours"),
                        jobJson.getBoolean("available_now"), payment, paymentPerHour, paymentPerMonth,
                        jobJson.getString("payment_rate"), jobJson.getBoolean("with_commission"),
                        jobJson.getBoolean("on_account"), jobJson.getBoolean("on_tax_card"),
                        studyFieldIdArray, languageIdArray, computerKnowledgeIdsArray, certificateIdsArray,
                        driversLicense, jobJson.getBoolean("without_locations"), locationsArray,
                        jobJson.getString("updated_at"), jobJson.getInt("object_version"),
                        jobJson.getString("public_url"));

                jobList.add(jobObj);
            }
            if (this.finishedLoadingJobDataListener != null) {
                this.finishedLoadingJobDataListener.onFinishLoadingJobListEvent(jobList);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setJobLoadDataListener(JobEventListener eventListener) {
        this.finishedLoadingJobDataListener = eventListener;
    }

    public void loadUserData() {

        String studentJson = loadJSONFromAsset();
        try {
            JSONObject studentObject = new JSONObject(studentJson);

            JSONObject addressObject = studentObject.getJSONObject("address");
            Address address = new Address(addressObject.getString("country"),
                    addressObject.getString("street_name"), addressObject.getString("street_number"),
                    addressObject.getString("postal_code"), addressObject.getString("city"));

            JSONObject resumeObject = studentObject.getJSONObject("resume");

            JSONArray studyFieldsJsonArray = resumeObject.getJSONArray("study_fields");
            ArrayList<String> studyFieldsArray = new ArrayList<>();
            for (int sf = 0; sf < studyFieldsJsonArray.length(); sf++) {
                studyFieldsArray.add(studyFieldsJsonArray.getString(sf));
            }

            JSONArray studiesJsonArray = resumeObject.getJSONArray("studies");
            ArrayList<String> studiesArray = new ArrayList<>();
            for (int s = 0; s < studiesJsonArray.length(); s++) {
                studiesArray.add(studiesJsonArray.getString(s));
            }

            JSONArray schoolsJsonArray = resumeObject.getJSONArray("studies");
            ArrayList<String> schoolsArray = new ArrayList<>();
            for (int sc = 0; sc < schoolsJsonArray.length(); sc++) {
                schoolsArray.add(schoolsJsonArray.getString(sc));
            }
            JSONArray professionalExperiencesJsonArray = resumeObject.getJSONArray("professional_experiences");
            ArrayList<ProfessionalExperience> professionalExperienceArray = new ArrayList<>();
            for (int pe = 0; pe < professionalExperiencesJsonArray.length(); pe++) {
                JSONObject profExpObject = professionalExperiencesJsonArray.getJSONObject(pe);
                professionalExperienceArray.add(new ProfessionalExperience(profExpObject.getString("company_name"),
                        profExpObject.getString("job_title"), profExpObject.getString("start_date"),
                        profExpObject.getString("end_date")));
            }

            JSONArray languagesJsonArray = resumeObject.getJSONArray("languages");
            ArrayList<Language> languageArray = new ArrayList<>();
            for (int l = 0; l < languagesJsonArray.length(); l++) {
                JSONObject languageObject = languagesJsonArray.getJSONObject(l);
                languageArray.add(new Language(languageObject.getInt("id"),
                        languageObject.getString("type"), languageObject.getString("level")));
            }

            JSONArray computerKnowledgeJsonArray = resumeObject.getJSONArray("computer_knowledges");
            ArrayList<ComputerKnowledge> computerKnowledgeArray = new ArrayList<>();
            for (int ck = 0; ck < computerKnowledgeJsonArray.length(); ck++) {
                JSONObject computerKnowledgeObject = computerKnowledgeJsonArray.getJSONObject(ck);
                computerKnowledgeArray.add(new ComputerKnowledge(computerKnowledgeObject.getInt("id"),
                        computerKnowledgeObject.getString("type"), computerKnowledgeObject.getString("level")));
            }

            JSONArray certificatesJsonArray = resumeObject.getJSONArray("certificates");
            ArrayList<String> certificatesArray = new ArrayList<>();
            for (int c = 0; c < certificatesJsonArray.length(); c++) {
                certificatesArray.add(certificatesJsonArray.getString(c));
            }

            JSONObject driversLicenseJson = resumeObject.getJSONObject("drivers_licence");
            DriversLicense driversLicense = new DriversLicense(driversLicenseJson.getBoolean("car"), driversLicenseJson.getBoolean("truck"),
                    driversLicenseJson.getBoolean("motorcycle"), driversLicenseJson.getBoolean("scooter"),
                    driversLicenseJson.getBoolean("has_car"), driversLicenseJson.getBoolean("passenger_transportation"),
                    driversLicenseJson.getBoolean("trailer"), driversLicenseJson.getBoolean("forklift"));

            Resume resume = new Resume(studyFieldsArray, studiesArray,
                    schoolsArray, professionalExperienceArray, languageArray,
                    computerKnowledgeArray, certificatesArray, driversLicense);

            StudentObject student = new StudentObject(studentObject.getInt("id"), studentObject.getString("external_key"),
                    studentObject.getString("type"), studentObject.getString("gender"),
                    studentObject.getString("first_name"), studentObject.getString("last_name"),
                    studentObject.getString("birthday"), studentObject.getString("email"),
                    address, studentObject.getString("mobile_number"), studentObject.getString("newsletter_unsubscription_url"),
                    resume);

            if (this.finishedLoadingStudentDataListener != null) {
                this.finishedLoadingStudentDataListener.onFinishLoadingStudentDataEvent(student);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setStudentLoadDataListener(StudentEventListener eventListener) {
        this.finishedLoadingStudentDataListener = eventListener;
    }
}
