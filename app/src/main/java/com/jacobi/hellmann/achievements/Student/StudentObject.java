package com.jacobi.hellmann.achievements.Student;

public class StudentObject {
    private int id;
    private String externalKey;
    private String updatedAt;
    private int objectVersion;
    private String type;
    private String gender;
    private String firstName;
    private String lastName;
    private String birthday;
    private String email;
    Address AddressObject;
    private String lastSignInAt;
    private String mobileNumber;
    private boolean wantsNewsletter;
    private boolean wantsSms;
    private String newsletterUnsubscriptionUrl;
    Resume ResumeObject;

    public StudentObject(int id, String externalKey, String type, String gender,
                         String firstName, String lastName, String birthday,
                         String email, Address addressObject, String mobileNumber,
                         String newsletterUnsubscriptionUrl, Resume resumeObject) {
        this.id = id;
        this.externalKey = externalKey;
        this.type = type;
        this.gender = gender;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.email = email;
        AddressObject = addressObject;
        this.mobileNumber = mobileNumber;
        this.newsletterUnsubscriptionUrl = newsletterUnsubscriptionUrl;
        ResumeObject = resumeObject;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getGender() {
        return gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getEmail() {
        return email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }
}

