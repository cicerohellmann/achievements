package com.jacobi.hellmann.achievements.Job;

public class PaymentPerHour {
    private int amount;
    private String currency;

    public PaymentPerHour(int amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public String getCompleteValue (){
        return String.valueOf(amount)+" "+currency;
    }
}