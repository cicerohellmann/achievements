package com.jacobi.hellmann.achievements.Util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

public class Date {

    String date;

    public Date(String dateForConversion) {
        this.date = dateForConversion;
    }

    public String dateConversion() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

        try {
            java.util.Date formatDate = format.parse(date);
            return DateFormat.getDateInstance(DateFormat.SHORT).format(formatDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }
}
