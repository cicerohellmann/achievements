package com.jacobi.hellmann.achievements.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.jacobi.hellmann.achievements.Util.JsonParser;
import com.jacobi.hellmann.achievements.R;
import com.jacobi.hellmann.achievements.Util.SharedPreferenceHandler;
import com.jacobi.hellmann.achievements.Student.StudentObject;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle(getResources().getString(R.string.login_activity));

        final Button button = findViewById(R.id.login_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                getUserData();

                Intent i = new Intent(getApplicationContext(), JobListingActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    private void getUserData() {
        JsonParser jasonStudentParser = new JsonParser(this, "user_2.json");

        jasonStudentParser.setStudentLoadDataListener(new JsonParser.StudentEventListener() {
            @Override
            public void onFinishLoadingStudentDataEvent(StudentObject student) {
                new SharedPreferenceHandler(getApplicationContext()).setUserInfo(student);
            }

        });
        jasonStudentParser.loadUserData();
    }
}
