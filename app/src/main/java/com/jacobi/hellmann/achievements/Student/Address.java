package com.jacobi.hellmann.achievements.Student;

public class Address {
    private String country;
    private String streetName;
    private String streetNumber;
    private String postalCode;
    private String city;

    public Address(String country, String streetName, String streetNumber, String postalCode, String city) {
        this.country = country;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.postalCode = postalCode;
        this.city = city;
    }
}