package com.jacobi.hellmann.achievements.Student;

public class ProfessionalExperience {
    private String companyName;
    private String jobTitle;
    private String startDate;
    private String endDate;

    public ProfessionalExperience(String companyName, String jobTitle, String startDate, String endDate) {
        this.companyName = companyName;
        this.jobTitle = jobTitle;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
