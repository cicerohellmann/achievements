package com.jacobi.hellmann.achievements.Student;

import com.jacobi.hellmann.achievements.CommonObjects.DriversLicense;

import java.util.ArrayList;

public class Resume {
    ArrayList<String> studyFields = new ArrayList<>();
    ArrayList<String> studies = new ArrayList<>();
    ArrayList<String> schools = new ArrayList<>();
    ArrayList<ProfessionalExperience> professionalExperiences = new ArrayList<>();
    ArrayList<Language> languages = new ArrayList<>();
    ArrayList<ComputerKnowledge> computerKnowledges = new ArrayList<>();
    ArrayList<String> certificates = new ArrayList<>();
    ArrayList<String> attachments = new ArrayList<>();
    DriversLicense driversLicence;

    public Resume(ArrayList<String> studyFields, ArrayList<String> studies,
                  ArrayList<String> schools, ArrayList<ProfessionalExperience> professionalExperiences,
                  ArrayList<Language> languages, ArrayList<ComputerKnowledge> computerKnowledges, ArrayList<String> certificates, DriversLicense driversLicence) {
        this.studyFields = studyFields;
        this.studies = studies;
        this.schools = schools;
        this.professionalExperiences = professionalExperiences;
        this.languages = languages;
        this.computerKnowledges = computerKnowledges;
        this.certificates = certificates;
        this.driversLicence = driversLicence;
    }
}