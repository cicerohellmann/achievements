package com.jacobi.hellmann.achievements.Ui;

import android.os.Bundle;
import android.view.View;

import com.jacobi.hellmann.achievements.Util.AchievementsHandler;
import com.jacobi.hellmann.achievements.R;

public class AchievementsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addContentView(R.layout.activity_achievements);

        AchievementsHandler achievements = new AchievementsHandler(this);
        if (achievements.isCompletedFirst()) {
            View view1 = this.findViewById(R.id.view1);
            view1.setVisibility(View.GONE);
        }
    }
}

