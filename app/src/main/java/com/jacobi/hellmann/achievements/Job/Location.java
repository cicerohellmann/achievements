package com.jacobi.hellmann.achievements.Job;

public class Location {
    private int id;
    private String city;
    private String district;
    private String country;
    private String postalCode;

    public Location(int id, String city, String district, String country, String postalCode) {
        this.id = id;
        this.city = city;
        this.district = district;
        this.country = country;
        this.postalCode = postalCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

}
