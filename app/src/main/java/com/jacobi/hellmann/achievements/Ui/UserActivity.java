package com.jacobi.hellmann.achievements.Ui;

import android.os.Bundle;
import android.widget.TextView;

import com.jacobi.hellmann.achievements.R;
import com.jacobi.hellmann.achievements.Util.SharedPreferenceHandler;

public class UserActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addContentView(R.layout.activity_user);

        TextView gender = this.findViewById(R.id.user_gender);
        TextView name = this.findViewById(R.id.user_first_second_name);
        TextView birthday = this.findViewById(R.id.user_birthday);
        TextView email = this.findViewById(R.id.user_email);
        TextView number = this.findViewById(R.id.user_mobile_number);

        SharedPreferenceHandler sph = new SharedPreferenceHandler(this);
        birthday.setText(sph.getUserBirthday());
        email.setText(sph.getUserEmail());
        name.setText(sph.getUserName());
        gender.setText(sph.getUserGender());
        number.setText(sph.getUserMobileNumber());

    }
}
