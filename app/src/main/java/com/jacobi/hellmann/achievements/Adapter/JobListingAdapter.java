package com.jacobi.hellmann.achievements.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jacobi.hellmann.achievements.Job.JobObject;
import com.jacobi.hellmann.achievements.R;

import java.util.ArrayList;

public class JobListingAdapter extends RecyclerView.Adapter<JobListingAdapter.MyViewHolder> {
    private ArrayList<JobObject> jobAdapterData;
    private OnItemClickListener itemClickListener;
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView jobTitle;
        public TextView workLoad;
        public TextView hourlyRate;
        public TextView expireDate;
        public TextView location;

        public MyViewHolder(View view, final OnItemClickListener listener) {
            super(view);
            this.jobTitle = view.findViewById(R.id.job_title);
            this.workLoad = view.findViewById(R.id.work_load);
            this.hourlyRate = view.findViewById(R.id.hourly_rate);
            this.expireDate = view.findViewById(R.id.expire_date);
            this.location = view.findViewById(R.id.location);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        itemClickListener = listener;
    }

    public JobListingAdapter(ArrayList<JobObject> myDataSet) {
        jobAdapterData = myDataSet;
    }

    @Override
    public JobListingAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.inflate(R.layout.cell_job_listing, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(view, itemClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.jobTitle.setText(jobAdapterData.get(position).getTitle());
        holder.workLoad.setText(jobAdapterData.get(position).getWorkingHoursString());
        holder.hourlyRate.setText(jobAdapterData.get(position).getPaymentPerHour().getCompleteValue());
        holder.expireDate.setText(jobAdapterData.get(position).getExpiresAt());
        holder.location.setText(jobAdapterData.get(position).getLocationsCity());

    }

    @Override
    public int getItemCount() {
        if(jobAdapterData !=null && jobAdapterData.size()>0)
            return jobAdapterData.size();
        return 0;
    }
}
