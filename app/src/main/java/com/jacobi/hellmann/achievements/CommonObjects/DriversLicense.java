package com.jacobi.hellmann.achievements.CommonObjects;

public class DriversLicense {
    private boolean car;
    private boolean truck;
    private boolean motorcycle;
    private boolean scooter;
    private boolean hasCar;
    private boolean passengerTransportation;
    private boolean trailer;
    private boolean forklift;

    public DriversLicense(boolean car, boolean truck, boolean motorcycle, boolean scooter, boolean hasCar, boolean passengerTransportation, boolean trailer, boolean forklift) {
        this.car = car;
        this.truck = truck;
        this.motorcycle = motorcycle;
        this.scooter = scooter;
        this.hasCar = hasCar;
        this.passengerTransportation = passengerTransportation;
        this.trailer = trailer;
        this.forklift = forklift;
    }
}
